betting.team my UI modfications
===============================

## About

This is a simple [userscript](https://github.com/OpenUserJs/OpenUserJS.org/wiki/Userscript-beginners-HOWTO) for [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/) or similar extension, which -- upon visiting a page on [betting.team](https:/betting.team) -- will do the following:

  - will conceal cents in balances;
  - will collapse all the expanded sections on the main Matches page (`l` and `h` will toggle this expanded state)
  - will redirect to the odds version of a particular match page;
  - on `x` key press, will switch between the Russian and the English versions.
