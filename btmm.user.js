// ==UserScript==
// @name        betting.team - my UI modifications
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-betting.team-my-ui-mods
// @downloadURL https://bitbucket.org/dsjkvf/userscript-betting.team-my-ui-mods/raw/master/btmm.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-betting.team-my-ui-mods/raw/master/btmm.user.js
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js
// @require     https://gist.github.com/raw/2625891/waitForKeyElements.js
// @version     1.13
// @match       https://betting.team/*
// @run-at      document-start
// @grant       none
// ==/UserScript==


// INIT

// Options
var myBackground = "#000000";
var myForeground = "#FFFFFF";

// Environment
var url = window.location.href;
var urlLastPart = url.substr(url.lastIndexOf('/') + 1);


// HELPERS

function stripCents(theTag) {
    /* strips cents from your balance data
    */

    // collect variables
    var reference = document.querySelector(theTag);
    // extract HTML
    var code = reference.innerHTML;
    // and alter it
    var rez = code.replace(/(\d\d\d\d*?)\.\d\d*/, "$1");

    // create a new element
    var a = document.createElement('a');
    // and fill it with our result
    a.innerHTML = rez;

    // do the replacement
    // clear the old contents of the reference
    reference.innerHTML = '';
    // append
    reference.appendChild(a);
};

function collapseAll() {
    /* collapses all the expanded subsections
    */

    var chapters = document.getElementsByClassName("is-open");
    var i = 0;
    while (i < chapters.length) {
        chapters[i].getElementsByClassName("ui-accordion__header-aside")[0].click();
        i++
    };
};

function expandAll() {
    /* expands all the collapsed subsections
    */

    /*
    may be also done this way:
    [...document.querySelectorAll("div.ui-accordion:not(.is-open)")].forEach(div => {div.getElementsByClassName('bi-hide-xs')[0].click(); })
    or this way:
    collapseAll();
    [...document.querySelectorAll("span.bi-hide-xs")].forEach(div => { div.click(); })
    still, the loop seems to be preferrable
    */

    var chapters = document.querySelectorAll("div.ui-accordion:not(.is-open)");
    var i = 0;
    while (i < chapters.length) {
        chapters[i].getElementsByClassName('ui-accordion__header-aside')[0].click();
        i++;
    };

    /*
    or this clumsy way:
    collapseAll()
    var chapters = document.getElementsByClassName("bi-hide-xs")
    var i = 0;
    while (i < chapters.length) {
        chapters[i].click()
        i++
    };
    */
};

function redirectToOdds(url) {
    /* adds a closing 'odds' if needed
    */

    if ( ! /odds\/$/.test (url) ) {
        return url + '/odds/';
    }
}

function myModsWrapper() {
    /* performs some UI tweaks
    */

    // set menu height
    document.getElementsByClassName("menu-desktop")[0].style.height='56px'

    // on a profile page, strip cents (don't really need this any more?)
    /*
    if ( url.indexOf("profile") !== -1 ) {
        stripCents('span.the-profile-header__balance');
    }
    */

    // on pages, which url ends with "match"
    // or which url contains "match" AND doesn't end with a digits only substring (meaning can end with a date),
    // collapse all the expanded subsections if possible
    if ( urlLastPart == "match" || (url.indexOf("match") !== -1 && ! /^\d+$/.test(urlLastPart) )) {
        collapseAll();
    }
};

function myColors() {
    // create style and apply it
    var myCSS = `
    .bi-bet:hover {
        background-color: ${myBackground} !important;
        color: ${myForeground} !important;
    };
    `
    var myStyle = document.createElement('style');

    if (myStyle.styleSheet) {
        myStyle.styleSheet.cssText = myCSS;
    } else {
        myStyle.appendChild(document.createTextNode(myCSS));
    }

    document.getElementsByTagName('head')[0].appendChild(myStyle);
};


// MAIN

// Tweak colors
// if an url contains "match"
// if ( url.indexOf("/match/") !== -1) {
myColors();
// }

// Proceed with other modifications as soon as the elements in question are loaded
waitForKeyElements("div.profile__balance", myModsWrapper );

// Hide their stupid chat sidebar as soon as possible
for (var i = 4000; i < 8000; i += 1000){
    setTimeout(function() {
        if (document.getElementsByClassName("curtain__btn")[0].innerHTML.indexOf("rotate") == -1){
            document.getElementsByClassName("curtain__btn")[0].click();
        }
    }, i);
};

// Add a hotkey to switch between Russian and English versions
addEventListener('keyup', function(e) {
    if ( e.ctrlKey ||
         e.altKey ||
         e.metaKey ||
         e.shiftKey ||
         (e.which !== 72 /*h*/ && e.which !== 76 /*l*/ && e.which !== 88 /*x*/ )) {
         return;
    }
    e.preventDefault();

    if (e.which == 76) {
        expandAll();
    } else if (e.which == 72) {
        collapseAll();
    } else if (e.which == 88) {
        var myRxRu = "/ru/";
        var myRxEn = "/en/";
        if (url.indexOf(myRxRu) !== -1) {
            window.location.replace(url.replace(myRxRu, myRxEn));
        } else if (url.indexOf(myRxEn) !== -1) {
            window.location.replace(url.replace(myRxEn, myRxRu));
        };
    };
})
